var ffAp = angular.module('ffApp', ['ui.sortable', 'ngRoute']);

// configure our routes
ffAp.config(function($routeProvider) {
    $routeProvider
        // route for the home page
        .when('/', {
            templateUrl : 'templates/products.html',
            controller  : 'sortableController'
        })

        // route for the about page
        .when('/about', {
            templateUrl : 'templates/about.html',
            controller  : 'sortableController'
        })

        // route for guide page
        .when('/guide', {
            templateUrl : 'templates/guide.html',
            controller  : 'sortableController'
        })
});

// Filter Model from wich all filters inherit and add functionality
ffAp.factory('Filter', [function() {
    /**
    * Represents the filters
    * @constuctor
    * @param {Array} items - items to be intstantiated to filter
    */
    function Filter(items) {
      this.items = items;
    };
    Filter.prototype = {
			items: [],
    };

    return Filter;
}]);

// Filter object from wich all filters inherit and add functionality
ffAp.factory('Products', [function() {
    /**
    * Represents the products
    * @constuctor
    * @param {Array} items - items to be intstantiated to productslist
    */
    function Products(items) {
      this.items = items;
    };
    Products.prototype = {
			items: [],
    };

    return Products;
}]);

// Available filter types
ffAp.factory('ChooseFilter', ['Filter', function(Filter) {
    /**
    * Represents the chosen filters
    * @constuctor
    * @param {Array} items - items to be intstantiated to chosen filters
    */
    function ChooseFilter(items) {
			this.items = items;
    };
    ChooseFilter.prototype = new Filter();

		ChooseFilter.prototype = {
        /**
        * Sets the amount of available products per filter
        * @param {Array} products - products to be added to filters
        */
				setAmount: function(products) {
						for (var d = 0; d < this.items.length; d++) {
							var filter = this.items[d];

							// loop trough products
							for (var j = 0; j < products.items.length; j++) {
								var product = products.items[j];

								for (var k = 0; k < product.specs.length; k++) {
									var spec = product.specs[k];

									if (spec.text === filter.text && spec.value === filter.value) {
										filter.amount = filter.amount + 1;
									}
								}
							}
						}
					}
				};

    return ChooseFilter;
}]);


// Available filter types
ffAp.factory('SelectedFilter', ['Filter', function(Filter) {
    /**
    * Represents the selected filters
    * @constuctor
    * @param {Array} items - items to be intstantiated to filter
    */
    function SelectedFilter(items) {
			this.items = items;
    };
    SelectedFilter.prototype = new Filter();

    return SelectedFilter;
}]);



ffAp.filter('score', function () {
	/**
  *  Custom angular filter function to filter and
  *  order products based on selected filters
  *  @param {Array} items - items to have score applied to
  *  @param {Array} hardFilters - used to filter out items
  *  @param {Array} softFilters - used to score items
  */
	return function (items, hardFilters, softFilters) {

    /**
    *  Order function to order products
    *  @param {Array} items - items to be ordered
    *  @param {string} field - field to be ordered on
    */
		var orderer = function(items, field) {
			items.sort(function (a, b) {
				return (a[field] < b[field] ? 1 : -1);
			});
      return items
		}

    /**
    *  Filter function to filter products
    *  @param {Array} items - items to be filtered
    *  @param {Array} hardFilters - filters to filter on
    */
		var hardFilterer = function(items, hardFilters) {
			var hardFiltered = [];

			// loop trough products
			for (var i = 0; i < items.length; i++) {
				var item = items[i];
				var match = true;

				// loop trough hardFilters
				for (var g = 0; (g < hardFilters.length) && match; g++) {
					var hardFilter = hardFilters[g];
					var match = false;

					if(!hardFilter.value) match = true;

					// loop trough product.specs
					for (var h = 0; (h < items[i].specs.length) && !match; h++) {
						var spec = items[i].specs[h];

						if (spec.text === hardFilter.text && spec.value === hardFilter.value) match = true;
						// match = angular.equals(spec, hardFilter);
					}
				}
				if (match) hardFiltered.push(item);
			}
			return hardFiltered;
		}

    /**
    *  Filter function to apply a rank on products
    *  based on softFilters
    *  order products based on selected filters
    *  @param {Array} items - items to be filtered
    *  @param {Array} softFilters - filters to filter on
    */
		var softFilterer = function(items, softFilters) {
			var softFiltered = [];

			// loop trough products
			for (var d = 0; d < items.length; d++) {
				var item = items[d]
				var score = 0;
				var rank = softFilters.length;

				// loop through softFilters
				for (var j = 0; j < softFilters.length; j++) {
					var softFilter = softFilters[j];

					if(softFilter.value) {
						// loop trough product.specs
						for (var k = 0; k < item.specs.length; k++ ) {
							var spec = item.specs[k];

							// check if softFilter matches spec
							if (spec.text === softFilter.text && spec.value === softFilter.value) {
								var match = true;

							} else {
								var match = false;
							}
							if (match) {
								score = score + rank;
							}
						}
						rank = rank - 1;
					}
				}
				item.score = score;
				softFiltered.push(item);
			}
			return softFiltered;
		}

		// run items trough filters and order them on score
		items = hardFilterer(items, hardFilters);
		items = softFilterer(items, softFilters);
		items = orderer(items, 'score');

		return items;
	};
});


ffAp.controller('sortableController', ['$scope', 'ChooseFilter', 'SelectedFilter', 'Products', function($scope, ChooseFilter, SelectedFilter, Products) {

  // initiate the fastFilters
	$scope.fastFilters = new ChooseFilter(
		[
			{
				text: 'HD Scherm',
				value: true,
				amount: 0
			},
			{
				text: 'Grote harde schijf',
				value: true,
				amount: 0
			},
			{
				text: 'Snelle processor',
				value: true,
				amount: 0
			},
			{
				text: 'Lange accuduur',
				value: true,
				amount: 0
			},
		]
	);

  // initiate the brandFilters
	$scope.brandFilters = new ChooseFilter(
		[
			{
				text: 'lenovo',
				value: true,
				amount: 0
			},
			{
				text: 'Asus',
				value: true,
				amount: 0
			},
			{
				text: 'Acer',
				value: true,
				amount: 0
			},
			{
				text: 'Toshiba',
				value: true,
				amount: 0
			},
		]
	);

  // initiate the screenFilters
	$scope.screenFilters = new ChooseFilter(
		[
			{
				text: '1200px x 1600px',
				value: true,
				amount: 0

			},
			{
				text: '1480px x 2200px',
				value: true,
				amount: 0
			},
		]
	);

  // initiate the softFilters
	$scope.softFilters = new SelectedFilter([]);

  // initiate the hardFilters
	$scope.hardFilters = new SelectedFilter([]);

  // initiate the products
	$scope.products = new Products([
  	{
  		name: 'Acer Aspire V2 BKWWX-STR',
  		score: 0,
  		price: 490,
  		image: 'http://img.cbcdn.net/products/356024?width=422&height=390',
  		specs: [
  		{
  			text: 'HD Scherm',
  			value: true
  		},
  		{
  			text: 'Grote harde schijf',
  			value: false
  		},
  		{
  			text: 'Snelle processor',
  			value: true
  		},
  		{
  			text: 'Lange accuduur',
  			value: false
  		},
  		{
  			text: '1480px x 2200px',
  			value: true,
  		},
  		{
  			text: 'Acer',
  			value: true
  		},
  		]
  	},
  	{
  		name: 'Asus Zenbook BX-SFJDTR',
  		score: 0,
  		price: 320,
  		image: 'http://img.cbcdn.net/products/364025?width=422&height=390',
  		specs: [
  		{
  			text: 'HD Scherm',
  			value: false
  		},
  		{
  			text: 'Grote harde schijf',
  			value: true
  		},
  		{
  			text: 'Snelle processor',
  			value: true
  		},
  		{
  			text: 'Lange accuduur',
  			value: true
  		},
  		{
  			text: 'Asus',
  			value: true
  		},
  		]
  	},
  	{
  		name: 'Asus Ultra SFJDTR',
  		score: 0,
  		price: 788,
  		image: 'http://img.cbcdn.net/products/371872?width=422&height=390',
  		specs: [
  		{
  			text: 'HD Scherm',
  			value: true
  		},
  		{
  			text: 'Grote harde schijf',
  			value: true
  		},
  		{
  			text: 'Snelle processor',
  			value: false
  		},
  		{
  			text: 'Lange accuduur',
  			value: false
  		},
  		{
  			text: 'Asus',
  			value: true
  		},
  		]
  	},
  	{
  		name: 'Toshiba Handybook BX-STR',
  		score: 0,
  		price: 890,
  		image: 'http://img.cbcdn.net/products/342788?width=422&height=390',
  		specs: [
  		{
  			text: 'HD Scherm',
  			value: true
  		},
  		{
  			text: 'Grote harde schijf',
  			value: true
  		},
  		{
  			text: 'Snelle processor',
  			value: false
  		},
  		{
  			text: 'Lange accuduur',
  			value: false
  		},
  		{
  			text: '1480px x 2200px',
  			value: true,
  		},
  		{
  			text: 'Toshiba',
  			value: true
  		},
  		]
  	},
  	{
  		name: 'Asus EliteNote 2 BR',
  		score: 1495,
  		price: 100,
  		image: 'http://img.cbcdn.net/products/359070?width=422&height=390',
  		specs: [
  		{
  			text: 'HD Scherm',
  			value: true
  		},
  		{
  			text: 'Grote harde schijf',
  			value: true
  		},
  		{
  			text: 'Snelle processor',
  			value: true
  		},
  		{
  			text: 'Lange accuduur',
  			value: false
  		},
  		{
  			text: 'Asus',
  			value: true
  		},
  		]
  	},
  	{
  		name: 'Acer Nova V6 BKWWX-STR',
  		score: 0,
  		price: 999,
  		image: 'http://img.cbcdn.net/products/375993?width=422&height=390',
  		specs: [
  		{
  			text: 'HD Scherm',
  			value: true
  		},
  		{
  			text: 'Grote harde schijf',
  			value: false
  		},
  		{
  			text: 'Snelle processor',
  			value: true
  		},
  		{
  			text: 'Lange accuduur',
  			value: true
  		},
  		{
  			text: 'Acer',
  			value: true
  		},
  		]
  	},
  	{
  		name: 'Lenovo BLRE-32789NL V5',
  		score: 0,
  		price: 689,
  		image: 'http://img.cbcdn.net/products/326747?width=422&height=390',
  		specs: [
  		{
  			text: 'HD Scherm',
  			value: true
  		},
  		{
  			text: 'Grote harde schijf',
  			value: true
  		},
  		{
  			text: 'Snelle processor',
  			value: false
  		},
  		{
  			text: 'Lange accuduur',
  			value: true
  		},
  		{
  			text: '1480px x 2200px',
  			value: true,
  		},
  		{
  			text: 'lenovo',
  			value: true
  		},
  		]
  	},
  	{
  		name: 'Lenovo Yoga 2 13-01133',
  		score: 0,
  		price: 739,
  		image: 'http://img.cbcdn.net/products/328763?width=422&height=390',
  		specs: [
  		{
  			text: 'HD Scherm',
  			value: false
  		},
  		{
  			text: 'Grote harde schijf',
  			value: false
  		},
  		{
  			text: 'Snelle processor',
  			value: true
  		},
  		{
  			text: 'Lange accuduur',
  			value: true
  		},
  		{
  			text: 'lenovo',
  			value: true
  		},
  		]
  	}
  ]);

  // Set amounts for all filters
	$scope.brandFilters.setAmount($scope.products);
	$scope.screenFilters.setAmount($scope.products);
	$scope.fastFilters.setAmount($scope.products);

  // Set the options for jQuery sortable
	$scope.sortableOptions = {
		containment: 'window',
		connectWith: '.sortable',
		axis: 'y',
		revert: 50,
	};

}]);
